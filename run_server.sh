#!/usr/bin/env bash
# Compiles the SASS files, because runtime compilation crashes for some reason
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
mkdir -p "$DIR/static/css" 2>&1 > /dev/null
sass "$DIR/static/sass/stylesheet.scss" "$DIR/static/css/stylesheet.css"
# Runs the server
sudo python3 manage.py runserver ${1:-svs.gyarab.cz:8000}
