var documentation = (function() {
  'use strict';

  let requestIndex = 0;

  function getFileURL(path) {
    return '/doc-raw/' + path;
  }

  function render(html) {
    $('#main-content').html(html);
  }

  function process(data) {
    return marked(_.escape(data));
  }

  function makeRequest(url) {
    requestIndex++;
    let currentRequestIndex = requestIndex;
    let isWanted = function() { return requestIndex === currentRequestIndex; };

    console.log(url);

    var promise = $.ajax({
      url: url
    })

    promise.done(function documentation__makeRequest__done(data, textStatus, jqXHR) {
      if(!isWanted())
        return;

      data = process(data);
      render(data);
    });

    promise.fail(function documentation__makeRequest__fail(jqXHR, textStatus, errorThrown) {
      if(!isWanted())
        return;

      render("Nepodařilo se načíst soubor.");
      console.error(errorThrown);
    });
  }

  function show(path) {
    let url = getFileURL(path);

    makeRequest(url);
  }

  return {
    show
  };
})();
