# AVAVA Website

## Requirements

* Python: 3.4.3+ (system-specific installation)
* Django: Tested on 1.9.1 (installation below)

## Installation

Run under root:

* pip install django
* pip install django\_compressor
* pip install django-appconf
* pip install django-libsass
* pip install django-allauth
* gem install sass

Log in to the administration at `/admin/` and do the following:

* Add a Site - https://docs.djangoproject.com/en/1.9/ref/contrib/sites/#enabling-the-sites-framework
* Add a Social application - https://django-allauth.readthedocs.org/en/latest/providers.html#django-configuration
