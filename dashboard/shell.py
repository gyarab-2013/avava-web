import subprocess


def call_command(args):
    print('Calling command: ' + ' '.join(str(x) for x in args))

    try:
        return subprocess.call(args)
    except Exception as e:
        raise CommandCallException('An error occurred while executing a command: ' + str(e))


def error_message_command(args):
    print('Calling command: ' + ' '.join(str(x) for x in args))

    try:
        # return subprocess.call(args)
        process = subprocess.Popen(args, stdout=subprocess.PIPE)
        out, err = process.communicate()

        if process.returncode == 0:
            return None
        else:
            return out
    except Exception as e:
        return e.message


def message_command(args):
    print('Calling command: ' + ' '.join(str(x) for x in args))

    try:
        # return subprocess.call(args)
        process = subprocess.Popen(args, stdout=subprocess.PIPE)
        out, err = process.communicate()
        return out
    except Exception as e:
        raise CommandCallException(e)


def if_try_command(args):
    try:
        return call_command(args) == 0
    except CommandCallException:
        return False


def if_command(args):
    return call_command(args) == 0


def assert_command(args):
    exit_code = call_command(args)

    if exit_code != 0:
        raise CommandCallException('The registration script exited unexpectedly with exit code: ' + exit_code)


class CommandCallException(Exception):
    '''Risen when an problem occurs while calling an external (non-python) script'''
