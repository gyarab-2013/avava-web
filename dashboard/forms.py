from django import forms
from django.core.validators import RegexValidator


class PostRegistrationForm(forms.Form):
    validator_username = RegexValidator(
        r'^[a-z]{3,8}$',
        'Uživatelské jméno musí obsahovat jen 3 až 8 malých písmen bez diakritiky.',
    )
    username = forms.CharField(
        label='Uživatelské jméno',
        help_text='Přihlašovací jméno ze 3 až 8 malých písmen bez diakritiky.',
        strip=True,
        validators=[validator_username, ],
    )


class PasswordChangeForm(forms.Form):
    pass
