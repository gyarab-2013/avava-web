import re
from django.dispatch import receiver
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from allauth.socialaccount.signals import pre_social_login
from allauth.exceptions import ImmediateHttpResponse


@receiver(pre_social_login)
def on_pre_social_login(request, sociallogin, **kwargs):
    regex = r"^(?P<name>[a-zA-Z0-9\.!#$%&'*+\/=?^_`{|}~-]+)@(?i)((student\.)?gyarab\.cz)$"
    pattern = re.compile(regex)
    email = str(sociallogin.user.email)
    match = pattern.match(email)

    if match is None:
        url = reverse('accounts-invalid')
        raise ImmediateHttpResponse(HttpResponseRedirect(url))
