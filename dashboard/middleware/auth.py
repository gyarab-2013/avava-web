from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse, get_resolver, resolve
from pprint import pprint
from dashboard.user_util import UserUtil

# An example of how a middleware may look
# class SessionMiddleware(object):
#     def __init__(self):
#         engine = import_module(settings.SESSION_ENGINE)
#         self.SessionStore = engine.SessionStore

#     def process_request(self, request):
#         session_key = request.COOKIES.get(settings.SESSION_COOKIE_NAME)
#         request.session = self.SessionStore(session_key)


# If the user has not filled out a post-authentication form,
# this middleware redirects them to it.
class PostAuthenticationMiddleware(object):
    def __init__(self):
        pass

    def process_request(self, request):
        # TODO: Allow certain pages (like Log out)
        allowed_url_names = [
            'accounts-post-registration-form',
            'account_logout',
        ]
        url_name = resolve(request.path_info).url_name
        if url_name in allowed_url_names:
            return
        if request.user.is_anonymous():
            return
        if request.user.is_superuser:
            return
        if UserUtil.is_registered(request.user):
            return

        url = reverse('accounts-post-registration-form')
        return HttpResponseRedirect(url)


# https://github.com/pennersr/django-allauth/issues/345
class DenyEmailEditingMiddleware:
    def __init__(self):
        pass

    def process_request(self, request):
        allowed_url_names = [
            'account_email',
        ]
        url_name = resolve(request.path_info).url_name
        if url_name in allowed_url_names:
            url = reverse('index')
            return HttpResponseRedirect(url)
