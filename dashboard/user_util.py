from dashboard.shell import error_message_command, if_command, message_command


class UserUtil:
    @staticmethod
    def register(form, request):
        return error_message_command([
            '../avava-scripts/register.sh',
            form.cleaned_data['username'],
            request.user.email,
        ])

    @staticmethod
    def is_registered(user):
        return if_command([
            '../avava-scripts/is_registered.sh',
            user.email,
        ])

    @staticmethod
    def get_username(user):
        return message_command([
            '../avava-scripts/get_username.sh',
            user.email,
        ])

    @staticmethod
    def generate_password(username):
        return message_command([
            '../avava-scripts/generate_password.sh',
            username,
        ])
