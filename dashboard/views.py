from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from dashboard.forms import PostRegistrationForm, PasswordChangeForm
from dashboard.user_util import UserUtil
from dashboard.shell import message_command

# Python version conflicts, see
# https://stackoverflow.com/questions/6594620/python-3-2-unable-to-import-urllib2-importerror-no-module-named-urllib2
try:
    import urllib.request as urllib2
except ImportError:
    import urllib2


def index(request):
    return render(request, 'dashboard/index.html', {
        'uname': message_command([
            'uname', '-a'
        ])
    })


def accounts_post_registration_form(request):
    # TODO do checks
    if request.method == 'POST':
        # Build a pre-filled form
        form = PostRegistrationForm(request.POST)

        # Validate received data
        if form.is_valid():
            # Run register script
            error_message = UserUtil.register(form, request)

            if error_message is None:
                # TODO: add a reverse relation
                return HttpResponseRedirect('accounts-generate-password')

            form.add_error(None, error_message)
    else:
        # Build a blank form
        form = PostRegistrationForm()

    return render(request, 'dashboard/accounts/post_registration_form.html', {
        'form': form,
    })


def accounts_invalid(request):
    return render(request, 'dashboard/accounts/invalid.html')


def accounts_info(request):
    if request.user.is_anonymous() or request.user.is_superuser:
        return HttpResponseRedirect(reverse('index'))

    return render(request, 'dashboard/accounts/info.html', {
        'username': UserUtil.get_username(request.user),
    })


def accounts_generate_password(request):
    if request.user.is_anonymous() or request.user.is_superuser:
        return HttpResponseRedirect(reverse('index'))

    username = UserUtil.get_username(request.user)
    args = {
        'username': username,
    }

    # TODO do checks
    if request.method == 'POST':
        # Build a pre-filled form
        form = PasswordChangeForm(request.POST)

        # Validate received data
        if form.is_valid():
            # Run register script
            password = UserUtil.generate_password(username)
            args['password'] = password

            return render(request, 'dashboard/accounts/password_generated.html', args)
    else:
        # Build a blank form
        form = PasswordChangeForm()

    args['form'] = form

    return render(request, 'dashboard/accounts/generate_password.html', args)


def doc_raw(request, path):
    """A proxy for documentation files hosted at GitLab.com

    Let's hope it doesn't hog up the server.

    """

    print(request)

    url = "https://gitlab.com/gyarab-2013/avava-doc/raw/master/" + path
    data = urllib2.urlopen(url).read()

    return HttpResponse(data, content_type="text/plain")


def doc(request, path):
    return render(request, 'dashboard/doc.html', {
        'path': path,
    })
