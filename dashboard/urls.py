from django.conf.urls import include, url
from django.views.generic import TemplateView
from dashboard import views

urlpatterns = [
    url(r'^accounts/', include('allauth.urls')),
    url(r'^accounts/invalid/$', views.accounts_invalid, name='accounts-invalid'),
    url(r'^accounts/post-register/$', views.accounts_post_registration_form, name='accounts-post-registration-form'),
    url(r'^accounts/info/$', views.accounts_info, name='accounts-info'),
    url(r'^accounts/generate-password/$', views.accounts_generate_password, name='accounts-generate-password'),
    url(r'^doc-raw/(.*)$', views.doc_raw, name='doc-raw'),
    url(r'^doc/(.*)$', views.doc, name='doc'),
    url(r'', views.index, name='index'),
]
