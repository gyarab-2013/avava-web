from django import forms
from django import template

register = template.Library()


@register.filter
def form_field_errors_concat(field):
    errors = field.errors

    if len(errors) <= 0:
        return ""

    result = errors[0]

    for error in errors:
        result += " " + error

    return result


@register.filter
def form_field_type(field):
    field = field.field  # Unwrap
    if isinstance(field, forms.EmailField):
        return "email"
    if isinstance(field, forms.CharField):
        return "text"
